const http = require("http");

const port = 3000;

const server = http.createServer((request, response) =>{

	if(request.url == "/login"){
		response.writeHead(200, {"Content-type": "text/plain"})
		response.end("Welcome to the Login Page")		
	} else {
		response.writeHead(404, {"Content-type": "text/plain"})
		response.end("I'M SORRY!! THE PAGE YOU ARE LOOKING FOR CANNOT BE FOUND!!!")
	}
});

server.listen(port);

console.log("Server is running at localhost:3000")